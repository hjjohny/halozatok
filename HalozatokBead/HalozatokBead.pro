#-------------------------------------------------
#
# Project created by QtCreator 2014-03-18T11:03:59
#
#-------------------------------------------------

QT       += core gui

TARGET = HalozatokBead
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp \
    mainwidget.cpp \
    drawerengine.cpp \
    field.cpp \
    position.cpp \
    sensor.cpp \
    sensorenvironment.cpp \
    simulation.cpp \
    sensorgraphicsitem.cpp \
    graphicsview.cpp

HEADERS  += mainwindow.h \
    mainwidget.h \
    drawerengine.h \
    field.h \
    position.h \
    sensor.h \
    sensorenvironment.h \
    simulation.h \
    sensorgraphicsitem.h \
    graphicsview.h

FORMS    += mainwindow.ui

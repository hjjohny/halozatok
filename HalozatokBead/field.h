#ifndef FIELD_H
#define FIELD_H

#include <vector>
#include "position.h"

class Field
{
public:
    Field(int radius);

    Position getRandomPosition() const;
    Position getRandomUnobstructedPosition() const;

    int getNumLayers() const;
    int getNumCells() const;

    bool isObstructed(const Position pos) const;
    void setObstructed(Position pos, bool isObstructed);

private:
    int _layers;
    std::vector<bool> _isObstructed;
};

#endif // FIELD_H

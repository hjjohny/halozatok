#include "position.h"
#include <QDebug>

Position::Position()
{
    set(0);
}

Position::Position(int zeroBasedIndex)
{
    set(zeroBasedIndex);
}

Position::Position(int layer, int inLayerIndex)
{
    set(layer, inLayerIndex);
}

Position::Position(int layer, int sector, int inSectorIndex)
{
    set(layer, sector, inSectorIndex);
}

bool Position::operator==(const Position &other) const
{
    return _layer == other._layer && _inLayerIndex == other._inLayerIndex;
}

bool Position::operator!=(const Position &other) const
{
    return !(*this == other);
}

int Position::getLayer() const
{
    return _layer;
}

int Position::getInLayerIndex() const
{
    return _inLayerIndex;
}

int Position::getZeroBasedIndex() const
{
    return _layer == 0 ? 0 : 1 + 3*(_layer-1)*_layer + _inLayerIndex;
}

int Position::getSector() const
{
    return _layer == 0 ? 0 : _inLayerIndex / _layer;
}

int Position::getInSectorIndex() const
{
    return _layer == 0 ? 0 : _inLayerIndex % _layer;
}

void Position::set(int layer, int sector, int inSectorIndex)
{
    _layer = layer;
    _inLayerIndex = sector * layer + inSectorIndex;
    if (!isValid(_layer, _inLayerIndex))
    {
        qDebug() << "Illegal position: layer " << _layer << ", in-layer index " << _inLayerIndex;
    }
}

void Position::set(int layer, int inLayerIndex)
{
    if (isValid(layer, inLayerIndex))
    {
        _layer = layer;
        _inLayerIndex = inLayerIndex;
    }
    else
    {
        qDebug() << "Illegal position: layer " << layer << ", in-layer index " << inLayerIndex;
    }
}

void Position::set(int zeroBasedIndex)
{
    if (zeroBasedIndex == 0)
    {
        _layer = 0;
        _inLayerIndex = 0;
    }
    else
    {
        --zeroBasedIndex;
        _layer = 1;
        while (zeroBasedIndex >= _layer*6) {
            zeroBasedIndex -= _layer*6;
            ++_layer;
        }
        _inLayerIndex = zeroBasedIndex;
    }
}

bool Position::isValid(int layer, int inLayerIndex)
{
    return layer == 0 ? inLayerIndex == 0 : inLayerIndex < layer*6;
}

void Position::test()
{
    int cntr = 0;
    for (int layer = 0; layer < 10; ++layer)
    {
        for (int inLayerIndex = 0; isValid(layer, inLayerIndex); ++inLayerIndex)
        {
            Position x(layer, inLayerIndex);
            int zeroBased = x.getZeroBasedIndex();
            if (zeroBased != cntr++)
            {
                qDebug() << "Error! Layer " << layer << ", in-layer index " << inLayerIndex
                         << "should be " << cntr-1 << " zero-based, and not" << zeroBased;
            }
            x.set(zeroBased);
            if (x.getLayer() != layer || x.getInLayerIndex() != inLayerIndex)
            {
                qDebug() << "Error! Zero-based index " << zeroBased << "should be layer " << layer
                         << ", in-layer index " << inLayerIndex << ", not " << x.getLayer() << ", " << x.getInLayerIndex();
            }
        }
    }
}

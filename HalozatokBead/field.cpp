#include "field.h"
#include <QDebug>
#include <cstdlib>
#include <ctime>

Field::Field(int radius) : _layers(radius+1), _isObstructed(1 + radius*(radius+1)*3)
{
    srand(time(NULL));
}

Position Field::getRandomPosition() const
{
    return Position(rand() % _isObstructed.size());
}

Position Field::getRandomUnobstructedPosition() const
{
    int res = 0;
    do {
        res = rand() % (int)_isObstructed.size();
    } while(_isObstructed[res]);
    return Position(res);
}

int Field::getNumLayers() const
{
    return _layers;
}

int Field::getNumCells() const
{
    return (int)_isObstructed.size();
}

bool Field::isObstructed(Position pos) const
{
    int index = pos.getZeroBasedIndex();
    if (index >= (int)_isObstructed.size())
    {
        qDebug() << "Layer " << pos.getLayer() << " is out of bounds";
    }
    return _isObstructed[index];
}

void Field::setObstructed(Position pos, bool isObstructed)
{
    int index = pos.getZeroBasedIndex();
    if (index >= (int)_isObstructed.size())
    {
        qDebug() << "Layer " << pos.getLayer() << " is out of bounds";
    }
    _isObstructed[index] = isObstructed;
}

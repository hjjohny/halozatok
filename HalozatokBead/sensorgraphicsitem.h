#ifndef SENSORGRAPHICSITEM_H
#define SENSORGRAPHICSITEM_H

#include <QGraphicsItem>
#include <QPen>

const int MAGIC_NUMBER = 3;

struct Arrow
{
  int directon;
  QPen pen;
  Arrow(){}
  Arrow(int direction_,
        const QPen& pen_=QPen(QBrush(QColor(Qt::green)),5,Qt::DotLine))
    :directon(direction_),pen(pen_){}
};

class SensorGraphicsItem : public QGraphicsItem
{
public:
  SensorGraphicsItem(qreal x_,qreal y_,int index_,int radius_, qreal penWidth_=1, QColor color = QColor(153,217,234));

  QRectF boundingRect() const;


  void paint(QPainter *painter, const QStyleOptionGraphicsItem *option,
               QWidget *widget);

private:
  int _index;
  int _radius;
  qreal _penWidth;
  QColor _color;
};


#endif // SENSORGRAPHICSITEM_H

#ifndef POSITION_H
#define POSITION_H

class Position
{
public:
    Position();
    Position(int zeroBasedIndex);
    Position(int layer, int inLayerIndex);
    Position(int layer, int sector, int inSectorIndex);

    bool operator==(const Position &other) const;
    bool operator!=(const Position &other) const;

    int getLayer() const;
    int getInLayerIndex() const;
    int getZeroBasedIndex() const;
    int getSector() const;
    int getInSectorIndex() const;

    void set(int layer, int sector, int inSectorIndex);
    void set(int layer, int inLayerIndex);
    void set(int zeroBasedIndex);

    static bool isValid(int layer, int inLayerIndex);
    static void test();

private:
    int _layer;
    int _inLayerIndex;
};

#endif // POSITION_H

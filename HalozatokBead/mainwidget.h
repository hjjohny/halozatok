#ifndef MAINWIDGET_H
#define MAINWIDGET_H

#include <QWidget>
#include <QPushButton>
#include <QGraphicsView>
#include <QRadioButton>
#include <QSpinBox>
#include <QCheckBox>
#include <QComboBox>
#include "drawerengine.h"
#include <QTimer>

class MainWidget : public QWidget
{
  Q_OBJECT
public:
  explicit MainWidget(QWidget *parent_ = 0);
  void setup();
signals:
public slots:
  void step();
  void start();
  void stop();
  void clickAt(const QPointF& pt);
  void reset();
  void resetSensors();
protected:
  void paintEvent(QPaintEvent *event);
private:
  QPushButton* _startButton;//automatikus lépkedés, ha akarunk
  QPushButton* _stepButton;//kézzel léptetés
  QPushButton* _stopButton;// megállítja az automatikus léptetést
  QGraphicsView* _graphicsView;//erre rajzol a drawerEngine
  DrawerEngine _drawerEngine;

  QTimer _stepTimer;
  Simulation _simulation;

  QRadioButton* clickObstructionButton;
  QRadioButton* clickAddSensorButton;
  QRadioButton* clickRemoveSensorButton;

  QPushButton* resetButton;
  QPushButton* resetSensorsButton;
  QSpinBox* layersSpinBox;
  QSpinBox* sensorsSpinBox;

  QCheckBox* showPressureCheckBox;
  QCheckBox* collisionAvoidanceCheckBox;
  QCheckBox* onlyGaCheckBox;
  QSpinBox* simSpeedSpinBox;
  QComboBox* simTypeComboBox;
};

#endif // MAINWIDGET_H

#include "drawerengine.h"
#include <QDebug>
#include <QBrush>
#include <QPolygonF>


TextItem::TextItem(qreal x_,qreal y_,const QString &text_)
  :_text(text_)
{
  setPos(x_,y_);
}

QRectF TextItem::boundingRect() const
{
  return QRectF(x(),y(),_text.size()*penSize,penSize*2);
}

void TextItem::paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget)
{
  QPen oldPen=painter->pen();
  painter->setPen(QPen(QBrush(Qt::black),20));
  painter->drawText(x(),y(),_text);
  painter->setPen(oldPen);
}


DrawerEngine::DrawerEngine(QGraphicsScene *scene_)
  :_scene(scene_)
{}

void DrawerEngine::setScene(QGraphicsScene* scene_)
{
  _scene=scene_;
}

void DrawerEngine::drawSimulation(Simulation &simulation_, bool showPressure, bool withField_, bool withSensorRadius_)
{
  _scene->clear();

  if(withField_)
  {
    drawField(simulation_.getField());
  }

  // szenzor kirajzolás
  const int numCells = simulation_.getField().getNumCells();
  for (int i = 0; i < numCells; ++i)
  {
      std::vector<Sensor> sensors=simulation_.getSensorsAt(Position(i));
      if(!sensors.empty())
      {
          drawSensor(sensors[0], showPressure);
      }
  }

  // nyilak kirajzolása
  std::vector< std::pair<Sensor, Position> > moves = simulation_.getPendingMoves();
  typedef std::vector< std::pair<Sensor, Position> >::const_iterator ittype;
  for (ittype it = moves.begin(); it != moves.end(); ++it)
  {
      drawArrow(it->first.getPosition(), it->second);
  }
}

void DrawerEngine::drawField(const Field &field_)
{
  const int layers=field_.getNumLayers();

  drawTT(layers);

  _scene->addItem(new TextItem(0,HEIGHT*layers/2+10,"S0"));
  _scene->addItem(new TextItem(layers*EDGE_LENGTH/2,HEIGHT/2*layers/2.,"S1"));
  _scene->addItem(new TextItem(layers*EDGE_LENGTH/2,-HEIGHT/2*layers/2,"S2"));
  _scene->addItem(new TextItem(0,-HEIGHT*layers/2-3,"S3"));
  _scene->addItem(new TextItem(-layers*EDGE_LENGTH/2,-HEIGHT/2*layers/2.,"S4"));
  _scene->addItem(new TextItem(-layers*EDGE_LENGTH/2,HEIGHT/2*layers/2.,"S5"));


  _scene->addItem(new TextItem(-layers*EDGE_LENGTH/2-10,0,"R5"));
  _scene->addItem(new TextItem(layers*EDGE_LENGTH/2+5,0,"R2"));
  _scene->addItem(new TextItem(-layers*EDGE_LENGTH/4.,-HEIGHT/2*layers-5,"R4"));
  _scene->addItem(new TextItem(layers*EDGE_LENGTH/4.+5,HEIGHT/2*layers+5,"R1"));
  _scene->addItem(new TextItem(layers*EDGE_LENGTH/4.+5,-HEIGHT/2*layers-5,"R3"));
  _scene->addItem(new TextItem(-layers*EDGE_LENGTH/4.-10,HEIGHT/2*layers+10,"R0"));

  //obstruction kiszámolás
  QColor obstrColor(139,69,19,160);
  const int numCells = field_.getNumCells();
  for (int i = 0; i < numCells; ++i)
  {
      Position pos(i);
      if(field_.isObstructed(pos))
      {
          QPointF o = calculatePosition(pos);
          _scene->addEllipse(o.x()-EDGE_LENGTH*0.55, o.y()-EDGE_LENGTH*0.55,
                             EDGE_LENGTH*1.1, EDGE_LENGTH*1.1, QPen(Qt::transparent), QBrush(obstrColor));
      }
  }

}

void DrawerEngine::drawSensor(const Sensor &sensor_, bool showPressure, const QVector<Arrow>& arrows_,bool withSensorRadius_)
{
  QPointF pos=calculatePosition(sensor_.getPosition());
  if(withSensorRadius_)
  {
    drawTT(1,pos.x(),pos.y(),false,QPen(QBrush(Qt::gray),5));
  }

  for(int i=0;i < arrows_.size();++i)
  {
    drawArrow(arrows_[i],pos.x(),pos.y());
  }

  QColor color;
  switch (sensor_.getState())
  {
  case Sensor::ADVANCE:
      color.setRgb(255, 0, 0);
      break;
  case Sensor::UNKNOWN:
      color.setRgb(153,217,234);
      break;
  case Sensor::ROTATION:
      color.setRgb(0,255,255);
      break;
  case Sensor::RETREAT:
      color.setRgb(255,255,255);
      break;
  case Sensor::FLOWING_CLOCKWISE:
      color.setRgb(0,0,255);
      break;
  case Sensor::FLOWING_DOWN:
      color.setRgb(0,0,255);
      break;
  case Sensor::FLOWING_CCLOCKWISE:
      color.setRgb(0,0,255);
      break;
  case Sensor::FLOWING_UP:
      color.setRgb(0,0,255);
      break;
  }

  _scene->addItem(new SensorGraphicsItem(pos.x()/2.,pos.y()/2.,showPressure ? sensor_.getPressure() : sensor_.getId(),15,1,color));
}

QPointF DrawerEngine::calculatePosition(const Position & pos_)
{
  int layer=pos_.getLayer();
  int sector=pos_.getSector();
  int inSector=pos_.getInSectorIndex();
  QPointF ret;

  //nem vagyok túl jó matekból uh ez van :D
  switch(sector)
  {
    case 0:
    {
      ret.setX((inSector-layer/2.)*EDGE_LENGTH);
      ret.setY(HEIGHT*layer);
      break;
    }
    case 1:
    {
      ret.setX((EDGE_LENGTH/2.)*(layer+inSector));
      ret.setY(HEIGHT*(layer-inSector));
      break;
    }
    case 2:
    {
      ret.setX((layer-inSector/2.)*EDGE_LENGTH);
      ret.setY(-HEIGHT*inSector);
      break;
    }
    case 3:
    {
      ret.setX((layer/2.-inSector)*EDGE_LENGTH);
      ret.setY(-HEIGHT*layer);
      break;
    }
    case 4:
    {
      ret.setX(-(EDGE_LENGTH/2.)*(layer+inSector));
      ret.setY(HEIGHT*(inSector-layer));
      break;
    }
    case 5:
    {
      ret.setX((inSector/2.-layer)*EDGE_LENGTH);
      ret.setY(HEIGHT*inSector);
      break;
    }
  }

  return ret;
}

void DrawerEngine::drawTT(int layers_, qreal centerX_, qreal centerY_,bool sectors_,const QPen& pen_)
{
  QPen sectorPen(QBrush(Qt::black),4);
  //drawing horizontal layer lines (S-es)
  if(sectors_)
  {
    _scene->addLine(centerX_-layers_*EDGE_LENGTH,centerY_,centerX_+layers_*EDGE_LENGTH,centerY_,sectorPen);
  }
  else
  {
    _scene->addLine(centerX_-layers_*EDGE_LENGTH,centerY_,centerX_+layers_*EDGE_LENGTH,centerY_,pen_);
  }
  for(int i=1;i <= layers_; ++i)
  {
    _scene->addLine(centerX_-(layers_-i/2.)*EDGE_LENGTH,centerY_+HEIGHT*i,
                    centerX_+(layers_-i/2.)*EDGE_LENGTH,centerY_+HEIGHT*i,pen_);

    _scene->addLine(centerX_-(layers_-i/2.)*EDGE_LENGTH,centerY_-HEIGHT*i,
                    centerX_+(layers_-i/2.)*EDGE_LENGTH,centerY_-HEIGHT*i,pen_);
  }
  //drawing 45° lines
  if(sectors_)
  {
    _scene->addLine(centerX_-layers_*EDGE_LENGTH/2.,centerY_-HEIGHT*layers_,
                  centerX_+layers_*EDGE_LENGTH/2.,centerY_+HEIGHT*layers_,sectorPen);
  }
  else
  {
    _scene->addLine(centerX_-layers_*EDGE_LENGTH/2.,centerY_-HEIGHT*layers_,
                  centerX_+layers_*EDGE_LENGTH/2.,centerY_+HEIGHT*layers_,pen_);
  }
  for(int i=1;i <= layers_; ++i)
  {
    _scene->addLine(centerX_-(layers_+i)*EDGE_LENGTH/2.,centerY_-HEIGHT*(layers_-i),
                    centerX_+layers_*EDGE_LENGTH/2.-i*EDGE_LENGTH,centerY_+HEIGHT*layers_,pen_);

    _scene->addLine(centerX_-layers_*EDGE_LENGTH/2.+i*EDGE_LENGTH,centerY_-HEIGHT*layers_,
                    centerX_+(layers_+i)*EDGE_LENGTH/2.,centerY_+HEIGHT*(layers_-i),pen_);
  }

  //drawing 135° lines
  if(sectors_)
  {
    _scene->addLine(centerX_+layers_*EDGE_LENGTH/2.,centerY_-HEIGHT*layers_,
                  centerX_-layers_*EDGE_LENGTH/2.,centerY_+HEIGHT*layers_,sectorPen);
  }
  else
  {
    _scene->addLine(centerX_+layers_*EDGE_LENGTH/2.,centerY_-HEIGHT*layers_,
                  centerX_-layers_*EDGE_LENGTH/2.,centerY_+HEIGHT*layers_,pen_);
  }
  for(int i=1;i <= layers_; ++i)
  {
    _scene->addLine(centerX_+(layers_+i)*EDGE_LENGTH/2.,centerY_-HEIGHT*(layers_-i),
                    centerX_-layers_*EDGE_LENGTH/2.+i*EDGE_LENGTH,centerY_+HEIGHT*layers_,pen_);

    _scene->addLine(centerX_+layers_*EDGE_LENGTH/2.-i*EDGE_LENGTH,centerY_-HEIGHT*layers_,
                    centerX_-(layers_+i)*EDGE_LENGTH/2.,centerY_+HEIGHT*(layers_-i),pen_);
  }
}

void DrawerEngine::drawArrow(const Position& from, const Position& to)
{
    QPointF fromP = calculatePosition(from);
    QPointF toP = calculatePosition(to);
    QPointF dir = toP - fromP;
    QPointF norm_dir(-dir.y(), dir.x());
    QPointF aBase = fromP + dir / 5.;
    QPointF aTop = toP - dir / 5.;
    QPointF aHead1 = fromP + (dir * 4. / 7.) + (norm_dir / 10.);
    QPointF aHead2 = fromP + (dir * 4. / 7.) - (norm_dir / 10.);
    QPen arrowPen(Qt::red);
    arrowPen.setWidthF(4.);
    _scene->addLine(aBase.x(), aBase.y(), aTop.x(), aTop.y(), arrowPen);
    _scene->addLine(aHead1.x(), aHead1.y(), aTop.x(), aTop.y(), arrowPen);
    _scene->addLine(aHead2.x(), aHead2.y(), aTop.x(), aTop.y(), arrowPen);
}

void DrawerEngine::drawArrow(const Arrow &arrow_,qreal x_,qreal y_)
{
  switch(arrow_.directon)
  {
    case 0:
    {
      _scene->addLine(x_,y_,x_-EDGE_LENGTH/2.,y_+HEIGHT,arrow_.pen);
      _scene->addLine(x_-EDGE_LENGTH/2.,y_+HEIGHT,x_-EDGE_LENGTH/2.+10,y_+HEIGHT,arrow_.pen);
      _scene->addLine(x_-EDGE_LENGTH/2.,y_+HEIGHT,x_-EDGE_LENGTH/2.-5,y_+HEIGHT-8,arrow_.pen);
      break;
    }
    case 1:
    {
      _scene->addLine(x_,y_,x_+EDGE_LENGTH/2.,y_+HEIGHT,arrow_.pen);
      _scene->addLine(x_+EDGE_LENGTH/2.,y_+HEIGHT,x_+EDGE_LENGTH/2.-12,y_+HEIGHT,arrow_.pen);
      _scene->addLine(x_+EDGE_LENGTH/2.,y_+HEIGHT,x_+EDGE_LENGTH/2.+6,y_+HEIGHT-13,arrow_.pen);
      break;
    }
    case 2:
    {
      _scene->addLine(x_,y_,x_+EDGE_LENGTH,y_,arrow_.pen);
      _scene->addLine(x_+EDGE_LENGTH,y_,x_+EDGE_LENGTH-5,y_-10,arrow_.pen);
      _scene->addLine(x_+EDGE_LENGTH,y_,x_+EDGE_LENGTH-5,y_+10,arrow_.pen);
      break;
    }
    case 3:
    {
      _scene->addLine(x_,y_,x_+EDGE_LENGTH/2.,y_-HEIGHT,arrow_.pen);
      _scene->addLine(x_+EDGE_LENGTH/2.,y_-HEIGHT,x_+EDGE_LENGTH/2.+8,y_-HEIGHT+14,arrow_.pen);
      _scene->addLine(x_+EDGE_LENGTH/2.,y_-HEIGHT,x_+EDGE_LENGTH/2.-12,y_-HEIGHT,arrow_.pen);
      break;
    }
    case 4:
    {
      _scene->addLine(x_,y_,x_-EDGE_LENGTH/2.,y_-HEIGHT,arrow_.pen);
      _scene->addLine(x_-EDGE_LENGTH/2.,y_-HEIGHT,x_-EDGE_LENGTH/2.-5,y_-HEIGHT+9,arrow_.pen);
      _scene->addLine(x_-EDGE_LENGTH/2.,y_-HEIGHT,x_-EDGE_LENGTH/2.+12,y_-HEIGHT,arrow_.pen);
      break;
    }
    case 5:
    {
      _scene->addLine(x_,y_,x_-EDGE_LENGTH,y_,arrow_.pen);
      _scene->addLine(x_-EDGE_LENGTH,y_,x_-EDGE_LENGTH+5,y_-10,arrow_.pen);
      _scene->addLine(x_-EDGE_LENGTH,y_,x_-EDGE_LENGTH+5,y_+10,arrow_.pen);
      break;
    }
  }

}



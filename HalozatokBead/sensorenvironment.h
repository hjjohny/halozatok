#ifndef SENSORENVIRONMENT_H
#define SENSORENVIRONMENT_H

#include "sensor.h"
#include "position.h"
#include <vector>

class Sensor;

class SensorEnvironment
{
public:

    class SingleCell
    {
    public:
        bool isObstructed;
        Position position;
        const std::vector<Sensor> *sensors;
        SingleCell() : isObstructed(true), sensors(0) {}
        SingleCell(bool obstr, Position pos, std::vector<Sensor> *snsr) : isObstructed(obstr), position(pos), sensors(snsr) {}
    };

    SensorEnvironment();

    std::vector<SingleCell> previousLayer;
    SingleCell clockwise;
    SingleCell current;
    SingleCell counterClockwise;
    std::vector<SingleCell> nextLayer;
};

#endif // SENSORENVIRONMENT_H

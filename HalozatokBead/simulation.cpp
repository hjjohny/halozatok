#include "simulation.h"
#include "position.h"
#include "sensor.h"
#include "sensorenvironment.h"
#include <QDebug>
#include <cstdlib>
#include <ctime>

Simulation::Simulation(Simulation::Options opts) :
    _type(opts.type), _field(opts.field), _sensorsAt(_field.getNumCells())
{
    srand(time(0));
    for (int i = 0; i < opts.sensors; ++i)
    {
        Position pos = _field.getRandomUnobstructedPosition();
        _sensorsAt[pos.getZeroBasedIndex()].push_back(Sensor(pos, Sensor::UNKNOWN));
    }
}

Field& Simulation::getField()
{
    return _field;
}

void Simulation::addSensorAt(const Position& pos, const Sensor& sensor)
{
    _pendingChanges.clear();
    _sensorsAt[pos.getZeroBasedIndex()].push_back(sensor);
}

void Simulation::removeSensorsAt(const Position& pos)
{
    _pendingChanges.clear();
    _sensorsAt[pos.getZeroBasedIndex()].clear();
}

std::vector<Sensor> Simulation::getSensorsAt(const Position &pos) const
{
    int index = pos.getZeroBasedIndex();
    if (index >= _field.getNumCells())
    {
        qDebug() << "Index " << index << " is out of range";
    }
    return _sensorsAt[pos.getZeroBasedIndex()];
}

std::vector< std::pair<Sensor, Position> > Simulation::getPendingMoves() const
{
    std::vector< std::pair<Sensor, Position> > res;
    typedef std::map<Sensor, std::pair<Position, Sensor::State> >::const_iterator ittype;
    for (ittype it = _pendingChanges.begin();
         it != _pendingChanges.end();
         ++it)
    {
        res.push_back(std::make_pair(it->first, it->second.first));
    }
    return res;
}

void Simulation::setType(Type type)
{
    _type = type;
}

void Simulation::step(bool collisionAvoidance)
{
    Sensor::collisionAvoidance(collisionAvoidance);
    // compute changes
    if (_pendingChanges.empty())
    {
        Sensor *sensorToUpdate = 0;
        int soFarSeen = 0;
        switch (_type)
        {
            case ONE_BY_ONE:
                for (unsigned i = 0; i < _sensorsAt.size(); ++i)
                {
                    for (unsigned j = 0; j < _sensorsAt[i].size(); ++j)
                    {
                        ++soFarSeen;
                        if (rand() % soFarSeen == 0)
                        {
                            sensorToUpdate = &_sensorsAt[i][j];
                        }
                    }
                }
                if (sensorToUpdate != 0)
                {
                    _pendingChanges[*sensorToUpdate] = sensorToUpdate->sense(prepareSensorEnvironment(*sensorToUpdate));
                }
                break;
            case ASYNCHRONOUS:
                for (unsigned i = 0; i < _sensorsAt.size(); ++i)
                {
                    for (unsigned j = 0; j < _sensorsAt[i].size(); ++j)
                    {
                        if (rand() % 5 == 0)
                        {
                            _pendingChanges[_sensorsAt[i][j]] = _sensorsAt[i][j].sense(prepareSensorEnvironment(_sensorsAt[i][j]));
                        }
                    }
                }
                break;
            case SYNCHRONOUS:
                for (unsigned i = 0; i < _sensorsAt.size(); ++i)
                {
                    for (unsigned j = 0; j < _sensorsAt[i].size(); ++j)
                    {
                        _pendingChanges[_sensorsAt[i][j]] = _sensorsAt[i][j].sense(prepareSensorEnvironment(_sensorsAt[i][j]));
                    }
                }
                break;
        }
    }
    else
    {
        // execute changes
        std::vector<std::vector<Sensor> > tmpSensorsAt(_sensorsAt.size());
        for (unsigned i = 0; i < _sensorsAt.size(); ++i)
        {
            for (unsigned j = 0; j < _sensorsAt[i].size(); ++j)
            {
                std::map<Sensor, std::pair<Position, Sensor::State> >::iterator it = _pendingChanges.find(_sensorsAt[i][j]);
                if (it != _pendingChanges.end())
                {
                    _sensorsAt[i][j].setPosition(it->second.first);
                    _sensorsAt[i][j].setState(it->second.second);
                    _sensorsAt[i][j].updatePressure();
                }
                tmpSensorsAt[_sensorsAt[i][j].getPosition().getZeroBasedIndex()].push_back(_sensorsAt[i][j]);
            }
        }
        _sensorsAt.swap(tmpSensorsAt);
        _pendingChanges.clear();
    }
}

SensorEnvironment Simulation::prepareSensorEnvironment(Sensor sensor)
{
    SensorEnvironment res;
    Position pos = sensor.getPosition();
    // Set current
    res.current = singleCellFrom(pos.getZeroBasedIndex());
    // Set clockwise
    int clockwiseIndex = pos.getInLayerIndex() == 0
            ? Position(pos.getLayer() + 1, 0).getZeroBasedIndex() - 1
            : pos.getZeroBasedIndex() - 1;
    res.clockwise = singleCellFrom(clockwiseIndex);
    // Set counter-clockwise
    int cclockwiseIndex = Position::isValid(pos.getLayer(), pos.getInLayerIndex() + 1)
            ? pos.getZeroBasedIndex() + 1
            : Position(pos.getLayer(), 0).getZeroBasedIndex();
    res.counterClockwise = singleCellFrom(cclockwiseIndex);
    // Set previous layer
    if (pos.getLayer() + 1 < _field.getNumLayers())
    {
        if (pos.getLayer() == 0) // middle position
        {
            for (int i = 1; i <= 6; ++i)
            {
                res.previousLayer.push_back(singleCellFrom(i));
            }
        }
        else
        {
            res.previousLayer.push_back(singleCellFrom(
                Position(pos.getLayer() + 1, pos.getSector(), pos.getInSectorIndex()).getZeroBasedIndex()));
            res.previousLayer.push_back(singleCellFrom(
                Position(pos.getLayer() + 1, pos.getSector(), pos.getInSectorIndex() + 1).getZeroBasedIndex()));
            if (pos.getInSectorIndex() == 0) // corner position
            {
                res.previousLayer.push_back(singleCellFrom(
                    Position(pos.getLayer() + 1, pos.getSector() == 0 ? 5 : pos.getSector() - 1, pos.getLayer()).getZeroBasedIndex()));
            }
        }
    }
    // Set next layer
    if (pos.getLayer() != 0)
    {
        if (pos.getInSectorIndex() == 0) // corner position
        {
            res.nextLayer.push_back(singleCellFrom(Position(pos.getLayer() - 1, pos.getSector(), 0).getZeroBasedIndex()));
        }
        else // inside sector
        {
            Position first = Position(pos.getLayer() - 1, pos.getSector(), pos.getInSectorIndex() - 1);
            res.nextLayer.push_back(singleCellFrom(first.getZeroBasedIndex()));
            if (Position::isValid(first.getLayer(), first.getInLayerIndex() + 1))
            {
                res.nextLayer.push_back(singleCellFrom(first.getZeroBasedIndex() + 1));
            }
            else
            {
                res.nextLayer.push_back(singleCellFrom(Position(first.getLayer(), 0).getZeroBasedIndex()));
            }
        }
    }
    return res;
}

SensorEnvironment::SingleCell Simulation::singleCellFrom(int positionIndex)
{
    SensorEnvironment::SingleCell cell;
    cell.position = Position(positionIndex);
    cell.isObstructed = _field.isObstructed(cell.position);
    cell.sensors = &_sensorsAt[positionIndex];
    return cell;
}

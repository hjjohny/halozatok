#include "graphicsview.h"
#include <QMouseEvent>

GraphicsView::GraphicsView(QWidget *parent) :
    QGraphicsView(parent)
{
}

void GraphicsView::mouseReleaseEvent (QMouseEvent *event)
{
    emit clickAt(event->posF());
}

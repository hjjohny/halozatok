#include "sensor.h"
#include <utility>

int Sensor::_nextId = 1;
bool Sensor::_collisionAvoidance = false;
bool Sensor::_onlyGreedyAdvance = false;

Sensor::Sensor()
    : _id(_nextId++), _curPosition(Position(0)), _curState(UNKNOWN), _rotationStart(0), _pressure(0), _nextPressure(0), _hitTheWall(false)
{
}

Sensor::Sensor(Position pos, Sensor::State state)
    : _id(_nextId++), _curPosition(pos), _curState(state), _rotationStart(0), _pressure(0), _nextPressure(0), _hitTheWall(false)
{
}

bool Sensor::operator==(const Sensor &other) const
{
    return _id == other._id;
}

bool Sensor::operator!=(const Sensor &other) const
{
    return _id != other._id;
}

bool Sensor::operator<(const Sensor &other) const
{
    return _id < other._id;
}

int Sensor::getId() const
{
    return _id;
}

Sensor::State Sensor::getState() const
{
    return _curState;
}

Position Sensor::getPosition() const
{
    return _curPosition;
}

void Sensor::setState(Sensor::State state)
{
    _curState = state;
}

void Sensor::setPosition(Position position)
{
    _curPosition = position;
}

int Sensor::getPressure() const
{
    return _pressure;
}

void Sensor::updatePressure()
{
    _pressure = _nextPressure;
    _nextPressure = 0;
}

std::pair<Position, Sensor::State> Sensor::sense(SensorEnvironment env)
{
    bool _prevHitTheWall = _hitTheWall;
    _hitTheWall = false;
    if (_curPosition.getLayer() != _rotationStart.getLayer() || _curState == FLOWING_UP || _curState == FLOWING_DOWN
            || _curState == FLOWING_CLOCKWISE || _curState == FLOWING_CCLOCKWISE)
    {
        _rotationStart.set(0);
    }
    // Handle collision
    if (env.current.sensors->size() > 1)
    {
        bool retreat = false;
        bool backwardRotateRetreat = false;
        for (unsigned j = 0; !retreat && j < env.current.sensors->size(); ++j)
        {
            const Sensor& other = env.current.sensors->at(j);
            if (other != *this &&
                      ((other.getState() == ROTATION && _curState == ADVANCE)
                    || (other.getState() != RETREAT && _curState == RETREAT)
                    || (other.getState() == _curState && other.getId() < _id)
                    || ((other.getState() == FLOWING_UP || (other.getState() == FLOWING_DOWN && _curState != ROTATION)
                             || other.getState() == FLOWING_CCLOCKWISE || other.getState() == FLOWING_CLOCKWISE)
                         && _curState != FLOWING_UP && _curState != FLOWING_DOWN
                             && _curState != FLOWING_CCLOCKWISE && _curState != FLOWING_CLOCKWISE))) {
                retreat = true;
            }
            if (other != *this && (_curState == ROTATE_RETREAT || (other.getState() == ROTATION && _curState == FLOWING_DOWN)))
            {
                backwardRotateRetreat = true;
            }

        }
        if (backwardRotateRetreat)
        {
            if (!env.clockwise.isObstructed)
            {
                std::make_pair(env.clockwise.position, ROTATE_RETREAT);
            }
            else
            {
                std::make_pair(_curPosition, ROTATE_RETREAT);
            }
        }
        if (retreat)
        {
            int best = -1;
            for (unsigned i = 1; i < env.previousLayer.size(); ++i)
            {
                SensorEnvironment::SingleCell& prevLayerCell = env.previousLayer[i];
                if (!prevLayerCell.isObstructed && (best == -1 || prevLayerCell.sensors->size() < env.previousLayer[best].sensors->size()))
                {
                    best = i;
                }
            }
            if (best > -1)
            {
                return std::make_pair(env.previousLayer[best].position, RETREAT);
            }
            else
            {
                if (!env.counterClockwise.isObstructed)
                {
                    return std::make_pair(env.counterClockwise.position, RETREAT);
                }
                else
                {
                    return std::make_pair(_curPosition, RETREAT);
                }
            }
        }
    }
    // Greedy Advance
    bool everybodyStuck = true;
    for (unsigned i = 0; !(_curState == FLOWING_UP) && i < env.nextLayer.size(); ++i)
    {
        SensorEnvironment::SingleCell& candidate = env.nextLayer[i];
        if (_collisionAvoidance && _curPosition.getLayer() == 1)
        {
            // Rule III-B.5 (Gateway Rule)
            if (_curPosition.getSector() == 0)
            {
                if (!candidate.isObstructed && candidate.sensors->size() == 0)
                {
                    return std::make_pair(candidate.position, ADVANCE);
                }
                else
                {
                    return std::make_pair(_curPosition, UNKNOWN);
                }
            }
            else
            {
                continue;
            }
        }
        if (candidate.isObstructed) continue;
        if (candidate.sensors->size() > 0)
        {
            for (unsigned j = 0; j < candidate.sensors->size(); ++j) {
                if (candidate.sensors->at(j).getState() == ADVANCE) {
                    everybodyStuck = false; } }
            continue;
        }
        if (_collisionAvoidance && _curPosition.getLayer() > 1)
        {
            // Rule III-B.3 (Edge Rule)
            if ((candidate.position.getSector() != _curPosition.getSector()
                 || candidate.position.getInSectorIndex() == _curPosition.getInSectorIndex())
                    && (_curState == FLOWING_DOWN || _curPosition.getInSectorIndex() != 0) // Rule III-B.4 (Corner Rule)
                    && (env.counterClockwise.sensors->size() == 0 || env.counterClockwise.sensors->at(0).getState() != FLOWING_DOWN))
                return std::make_pair(candidate.position, ADVANCE);
        }
        else
        {
            // Rule III-A.1 (Priority Rule)
            if (env.counterClockwise.sensors->size() > 0
                    && ((candidate.position.getInSectorIndex() == _curPosition.getInSectorIndex()
                            && candidate.position.getSector() == _curPosition.getSector()
                            && _curPosition.getInSectorIndex() != 0)
                        || candidate.position.getSector() != _curPosition.getSector())
                    && _curPosition.getLayer() > 1) // Rule III-A.3 (Innermost-Layer Rule)
                continue;
            // Rule III-A.2 (Forbiddance Rule)
            if (_curState != FLOWING_DOWN && _curPosition.getInSectorIndex() == 1 && candidate.position.getInSectorIndex() == 0
                    && _curPosition.getSector() == candidate.position.getSector())
                continue;
            return std::make_pair(candidate.position, ADVANCE);
        }
    }
    // Greedy-Rotation-Greedy
    if (_onlyGreedyAdvance) return std::make_pair(_curPosition, UNKNOWN);
    if (_curState != FLOWING_CLOCKWISE && _curState != FLOWING_DOWN && everybodyStuck && _curPosition != _rotationStart && _curPosition.getLayer() != 0
            && env.counterClockwise.sensors->size() == 0 && !env.counterClockwise.isObstructed)
    {
        // Rule III-B.1 (Suspension Rule)
        bool suspensionRule = false;
        for (unsigned i = 0; !suspensionRule && i < env.nextLayer.size(); ++i)
        {
            SensorEnvironment::SingleCell& nextLayerCell = env.nextLayer[i];
            for (unsigned j = 0; !suspensionRule && j < nextLayerCell.sensors->size(); ++j)
            {
                if (nextLayerCell.sensors->at(j).getState() == ROTATION)
                {
                    _rotationStart.set(0);
                    suspensionRule = true;
                }
            }
        }
        if (!suspensionRule)
        {
            // Rule III-B.2 (Competition Rule)
            bool competitionRule = false;
            for (unsigned i = 0; !competitionRule && i < env.previousLayer.size(); ++i)
            {
                SensorEnvironment::SingleCell& prevLayerCell = env.previousLayer[i];
                for (unsigned j = 0; !competitionRule && j < prevLayerCell.sensors->size(); ++j)
                {
                    if (prevLayerCell.sensors->at(j).getPosition().getInSectorIndex() == _curPosition.getInSectorIndex() + 1
                            && prevLayerCell.sensors->at(j).getPosition().getSector() == _curPosition.getSector())
                    {
                        competitionRule = true;
                    }
                }
            }
            if (!competitionRule)
            {
                if (_rotationStart.getLayer() != _curPosition.getLayer()) _rotationStart = _curPosition;
                return std::make_pair(env.counterClockwise.position, ROTATION);
            }
        }
    }
    // Flowing behavior
    // gather information
    int nextHops = env.nextLayer.size();
    int obstNextHops = 0;
    for (unsigned i = 0; i < env.nextLayer.size(); ++i) if (env.nextLayer.at(i).isObstructed) ++obstNextHops;
    int prevHops = env.previousLayer.size();
    int obstPrevHops = 0;
    for (unsigned i = 0; i < env.previousLayer.size(); ++i) if (env.previousLayer.at(i).isObstructed) ++obstPrevHops;
    int pressureFromAbove = 0;
    if (obstPrevHops > 0 || env.counterClockwise.isObstructed)
        for (unsigned i = 0; i < env.previousLayer.size(); ++i)
            for (unsigned j = 0; j < env.previousLayer.at(i).sensors->size(); ++j)
                if (env.previousLayer.at(i).sensors->at(j).getState() == FLOWING_DOWN
                        && env.previousLayer.at(i).sensors->at(j).getPressure() > pressureFromAbove)
                    pressureFromAbove = env.previousLayer.at(i).sensors->at(j).getPressure();
    int pressureFromCClockwise = 0;
    for (unsigned j = 0; j < env.counterClockwise.sensors->size(); ++j)
        if (env.counterClockwise.sensors->at(j).getState() == FLOWING_CLOCKWISE
                && env.counterClockwise.sensors->at(j).getPressure() > pressureFromCClockwise)
            pressureFromCClockwise = env.counterClockwise.sensors->at(j).getPressure();
    int pressureFromClockwise = 0;
    for (unsigned j = 0; j < env.clockwise.sensors->size(); ++j)
        if (env.clockwise.sensors->at(j).getState() == FLOWING_CCLOCKWISE
                && env.clockwise.sensors->at(j).getPressure() > pressureFromClockwise)
            pressureFromClockwise = env.clockwise.sensors->at(j).getPressure();
    int pressureFromBelow = 0;
    if (obstNextHops > 0 || env.clockwise.isObstructed)
        for (unsigned i = 0; i < env.nextLayer.size(); ++i)
            for (unsigned j = 0; j < env.nextLayer.at(i).sensors->size(); ++j)
                if (env.nextLayer.at(i).sensors->at(j).getState() == FLOWING_UP
                        && env.nextLayer.at(i).sensors->at(j).getPressure() > pressureFromBelow)
                    pressureFromBelow = env.nextLayer.at(i).sensors->at(j).getPressure();
    int verticalMinPressure = pressureFromAbove < pressureFromBelow ? pressureFromAbove : pressureFromBelow;
    int horizontalMinPressure = pressureFromCClockwise < pressureFromClockwise ? pressureFromCClockwise : pressureFromClockwise;
    pressureFromAbove -= verticalMinPressure;
    pressureFromBelow -= verticalMinPressure;
    pressureFromCClockwise -= horizontalMinPressure;
    pressureFromClockwise -= horizontalMinPressure;
    // act on information
    State flowDirection = UNKNOWN;
    if (pressureFromBelow > 1)
    {
        _nextPressure = pressureFromBelow - 1;
        flowDirection = !env.clockwise.isObstructed ? FLOWING_CLOCKWISE : obstPrevHops < prevHops ? FLOWING_UP : FLOWING_CCLOCKWISE;
    }
    if (pressureFromClockwise > 0)
    {
        _nextPressure = pressureFromClockwise;
        flowDirection = obstPrevHops < prevHops ? FLOWING_UP : FLOWING_CCLOCKWISE;
    }
    if (pressureFromCClockwise > 0)
    {
        _nextPressure = pressureFromCClockwise;
        flowDirection = obstNextHops < nextHops ? FLOWING_DOWN : FLOWING_CLOCKWISE;
    }
    if (pressureFromAbove > 0)
    {
        _nextPressure = pressureFromAbove + 1;
        flowDirection = !env.counterClockwise.isObstructed ? FLOWING_CCLOCKWISE : obstNextHops < nextHops ? FLOWING_DOWN : FLOWING_CLOCKWISE;
    }
    if (flowDirection == FLOWING_UP && env.previousLayer.size() > 0 && _nextPressure > 2)
    {
            int best = -1;
            for (unsigned i = 0; i < env.previousLayer.size(); ++i)
            {
                SensorEnvironment::SingleCell& prevLayerCell = env.previousLayer[i];
                if (!prevLayerCell.isObstructed &&
                        (best == -1 || prevLayerCell.position.getInLayerIndex() < env.previousLayer[best].position.getInLayerIndex()))
                {
                    best = i;
                }
            }
            if (best != -1 && env.previousLayer[best].sensors->size() == 0)
                return std::make_pair(env.previousLayer[best].position, flowDirection);
    }
    if ((_nextPressure == 0 && (env.counterClockwise.isObstructed || _prevHitTheWall))
            || (_curState == FLOWING_CLOCKWISE && !env.clockwise.isObstructed && env.clockwise.sensors->size() == 0))
    {
        _nextPressure = _pressure == 0 ? 1 : _pressure;
        _hitTheWall = true;
        if (obstNextHops == nextHops)
        {
            if(env.clockwise.sensors->size() == 0 && !env.clockwise.isObstructed)
            {
                return std::make_pair(env.clockwise.position, FLOWING_CLOCKWISE);
            }
            else
            {
                return std::make_pair(_curPosition, FLOWING_CLOCKWISE);
            }
        }
        else
        {
            return std::make_pair(_curPosition, FLOWING_DOWN);
        }
    }
    return std::make_pair(_curPosition, flowDirection);
}

void Sensor::resetId()
{
    Sensor::_nextId = 1;
}

void Sensor::collisionAvoidance(bool value)
{
    _collisionAvoidance = value;
}

void Sensor::onlyGreedyAdvance(bool value)
{
    _onlyGreedyAdvance = value;
}

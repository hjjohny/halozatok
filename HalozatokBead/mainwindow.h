#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include "mainwidget.h"
#include "simulation.h"

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
  Q_OBJECT
  
public:
  explicit MainWindow(QWidget *parent_= 0);
  ~MainWindow();

public slots:
private:
  Ui::MainWindow *_ui;
  MainWidget* _mainWidget;
};

#endif // MAINWINDOW_H

#include "mainwidget.h"
#include <QHBoxLayout>
#include <QVBoxLayout>
#include "field.h"
#include "simulation.h"
#include "position.h"
#include <QMouseEvent>
#include <QDebug>
#include <QGroupBox>
#include <QLabel>
#include "graphicsview.h"

MainWidget::MainWidget(QWidget *parent_) :
    QWidget(parent_), _stepTimer(this), _simulation(Simulation::Options())
{
  setup();
}

void MainWidget::setup()
{
  _graphicsView=new GraphicsView(this);
  QGraphicsScene* scene= new QGraphicsScene();
  _drawerEngine.setScene(scene);
  _graphicsView->setScene(scene);
  connect(_graphicsView, SIGNAL(clickAt(QPointF)), this, SLOT(clickAt(QPointF)));

  QHBoxLayout* mainLayout = new QHBoxLayout();

  QVBoxLayout* buttonLayout = new QVBoxLayout();
  connect(&_stepTimer, SIGNAL(timeout()), this, SLOT(step()));
  _startButton = new QPushButton("Start",this);
  connect(_startButton, SIGNAL(clicked()), this, SLOT(start()));
  _stepButton = new QPushButton("Step",this);
  connect(_stepButton, SIGNAL(clicked()), this, SLOT(step()));
  _stopButton = new QPushButton("Stop",this);
  connect(_stopButton, SIGNAL(clicked()), this, SLOT(stop()));
  buttonLayout->addWidget(_startButton);
  buttonLayout->addWidget(_stepButton);
  buttonLayout->addWidget(_stopButton);

  QGroupBox *radioGroup = new QGroupBox(this);
  QVBoxLayout *radioLayout = new QVBoxLayout();
  clickObstructionButton = new QRadioButton("Modify obstruction", this);
  clickAddSensorButton = new QRadioButton("Add sensors", this);
  clickRemoveSensorButton = new QRadioButton("Remove sensors", this);
  radioLayout->addWidget(clickObstructionButton);
  radioLayout->addWidget(clickAddSensorButton);
  radioLayout->addWidget(clickRemoveSensorButton);
  radioGroup->setLayout(radioLayout);

  QVBoxLayout *simLayout = new QVBoxLayout();
  QHBoxLayout *resetLayout = new QHBoxLayout();
  resetButton = new QPushButton("Reset simulation", this);
  connect(resetButton, SIGNAL(clicked()), this, SLOT(reset()));
  resetSensorsButton = new QPushButton("Reset sensors", this);
  connect(resetSensorsButton, SIGNAL(clicked()), this, SLOT(resetSensors()));
  resetLayout->addWidget(resetButton);
  resetLayout->addWidget(resetSensorsButton);
  QHBoxLayout *layersLayout = new QHBoxLayout();
  QLabel *layersLabel = new QLabel("Layers", this);
  layersSpinBox = new QSpinBox(this);
  layersSpinBox->setValue(4);
  layersLayout->addWidget(layersLabel);
  layersLayout->addWidget(layersSpinBox);
  QHBoxLayout *sensorsLayout = new QHBoxLayout();
  QLabel *sensorsLabel = new QLabel("Sensors", this);
  sensorsSpinBox = new QSpinBox(this);
  sensorsSpinBox->setValue(5);
  sensorsLayout->addWidget(sensorsLabel);
  sensorsLayout->addWidget(sensorsSpinBox);
  simLayout->addLayout(resetLayout);
  simLayout->addLayout(layersLayout);
  simLayout->addLayout(sensorsLayout);

  QVBoxLayout *optsLayout = new QVBoxLayout();
  showPressureCheckBox = new QCheckBox("Show pressure", this);
  optsLayout->addWidget(showPressureCheckBox);
  collisionAvoidanceCheckBox = new QCheckBox("Collision avoidance", this);
  optsLayout->addWidget(collisionAvoidanceCheckBox);
  onlyGaCheckBox = new QCheckBox("Only GA", this);
  optsLayout->addWidget(onlyGaCheckBox);
  QHBoxLayout *speedLayout = new QHBoxLayout();
  QLabel *speedLabel = new QLabel("Speed (ms wait)", this);
  simSpeedSpinBox = new QSpinBox(this);
  simSpeedSpinBox->setMinimum(10);
  simSpeedSpinBox->setMaximum(10000);
  simSpeedSpinBox->setValue(500);
  simSpeedSpinBox->setSingleStep(10);
  connect(simSpeedSpinBox, SIGNAL(valueChanged(int)), &_stepTimer, SLOT(start(int)));
  speedLayout->addWidget(speedLabel);
  speedLayout->addWidget(simSpeedSpinBox);
  optsLayout->addLayout(speedLayout);
  simTypeComboBox = new QComboBox(this);
  simTypeComboBox->addItem("SYNCHRONOUS");
  simTypeComboBox->addItem("ASYNCHRONOUS");
  simTypeComboBox->addItem("ONE_BY_ONE");
  simTypeComboBox->setCurrentIndex(0);
  optsLayout->addWidget(simTypeComboBox);

  QVBoxLayout *controlLayout = new QVBoxLayout();
  controlLayout->addLayout(buttonLayout);
  controlLayout->addLayout(simLayout);
  controlLayout->addLayout(optsLayout);
  controlLayout->addWidget(radioGroup);
  controlLayout->addStretch();

  mainLayout->addWidget(_graphicsView);
  mainLayout->addLayout(controlLayout);

  setLayout(mainLayout);

  // initialize simulation
  reset();
}

void MainWidget::start()
{
    _stepTimer.start(simSpeedSpinBox->value());
}

void MainWidget::stop()
{
    _stepTimer.stop();
}

void MainWidget::step()
{
    if (simTypeComboBox->currentText() == "SYNCHRONOUS")
    {
        _simulation.setType(Simulation::SYNCHRONOUS);
    }
    else if (simTypeComboBox->currentText() == "ASYNCHRONOUS")
    {
        _simulation.setType(Simulation::ASYNCHRONOUS);
    }
    else if (simTypeComboBox->currentText() == "ONE_BY_ONE")
    {
        _simulation.setType(Simulation::ONE_BY_ONE);
    }
    Sensor::onlyGreedyAdvance(onlyGaCheckBox->isChecked());
    _simulation.step(collisionAvoidanceCheckBox->isChecked());
    update();
}

void MainWidget::reset()
{
    Sensor::resetId();
    Field field(layersSpinBox->value());
    Simulation::Options opts;
    opts.field = field;
    opts.sensors = sensorsSpinBox->value();
    opts.type = Simulation::SYNCHRONOUS;
    _simulation = Simulation(opts);
    update();
}

void MainWidget::resetSensors()
{
    Sensor::resetId();
    Simulation::Options opts;
    opts.field = _simulation.getField();
    opts.sensors = sensorsSpinBox->value();
    opts.type = Simulation::SYNCHRONOUS;
    _simulation = Simulation(opts);
    update();
}

void MainWidget::clickAt(const QPointF& pt)
{
    QPointF relativePos = pt - QPointF(_graphicsView->width() / 2., _graphicsView->height() / 2.);
    for (int i = 0; i < _simulation.getField().getNumCells(); ++i)
    {
        Position pos(i);
        QPointF ctr = _drawerEngine.calculatePosition(pos);
        QPointF distVec = ctr - relativePos;
        qreal dist = distVec.x()*distVec.x() + distVec.y()*distVec.y();
        if (dist < 800.)
        {
            if (clickObstructionButton->isChecked())
            {
                _simulation.removeSensorsAt(pos);
                _simulation.getField().setObstructed(pos, !_simulation.getField().isObstructed(pos));
            }
            else if (clickAddSensorButton->isChecked())
            {
                _simulation.addSensorAt(pos, Sensor(pos, Sensor::UNKNOWN));
            }
            else if (clickRemoveSensorButton->isChecked())
            {
                _simulation.removeSensorsAt(pos);
            }
            update();
            break;
        }
    }
}

void MainWidget::paintEvent(QPaintEvent *event)
{
    _drawerEngine.drawSimulation(_simulation, showPressureCheckBox->isChecked());
}

#ifndef SIMULATION_H
#define SIMULATION_H

#include "field.h"
#include "sensor.h"
#include "sensorenvironment.h"
#include <vector>
#include <map>

class Simulation
{
public:

    enum Type { ONE_BY_ONE, ASYNCHRONOUS, SYNCHRONOUS };

    class Options {
    public:
        Field field;
        int sensors;
        Type type;

        Options() : field(0), sensors(0), type(ONE_BY_ONE) {}
    };

    Simulation(Simulation::Options opts);

    Field& getField();
    void addSensorAt(const Position& pos, const Sensor& sensor);
    void removeSensorsAt(const Position& pos);
    std::vector<Sensor> getSensorsAt(const Position &pos) const;
    std::vector< std::pair<Sensor, Position> > getPendingMoves() const;

    void setType(Type type);

    void step(bool collisionAvoidance = false);

private:
    Type _type;
    Field _field;
    std::vector<std::vector<Sensor> > _sensorsAt;
    std::map<Sensor, std::pair<Position, Sensor::State> > _pendingChanges;

    SensorEnvironment prepareSensorEnvironment(Sensor sensor);
    SensorEnvironment::SingleCell singleCellFrom(int positionIndex);
};

#endif // SIMULATION_H

#ifndef SENSOR_H
#define SENSOR_H

#include "position.h"
#include "sensorenvironment.h"
#include <utility>

class SensorEnvironment;

class Sensor
{
public:
    enum State { UNKNOWN, ADVANCE, ROTATION, RETREAT, FLOWING_CCLOCKWISE, FLOWING_CLOCKWISE, FLOWING_DOWN, FLOWING_UP, ROTATE_RETREAT };

    Sensor();
    Sensor(Position pos, Sensor::State state);

    bool operator==(const Sensor &other) const;
    bool operator!=(const Sensor &other) const;
    bool operator<(const Sensor &other) const;

    int getId() const;

    Sensor::State getState() const;
    Position getPosition() const;

    void setState(Sensor::State state);
    void setPosition(Position position);

    int getPressure() const;
    void updatePressure();

    std::pair<Position, Sensor::State> sense(SensorEnvironment env);

    static void resetId();
    static void collisionAvoidance(bool value);
    static void onlyGreedyAdvance(bool value);

private:
    int _id;
    Position _curPosition;
    Sensor::State _curState;
    Position _rotationStart;
    int _pressure;
    int _nextPressure;
    bool _hitTheWall;

    static int _nextId;
    static bool _collisionAvoidance;
    static bool _onlyGreedyAdvance;
};

#endif // SENSOR_H

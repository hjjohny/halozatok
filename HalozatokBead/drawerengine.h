#ifndef DRAWERENGINE_H
#define DRAWERENGINE_H

#include <cmath>
#include <QWidget>
#include <QPainter>
#include <QGraphicsScene>
#include "simulation.h"
#include "sensorgraphicsitem.h"


const int EDGE_LENGTH = 60;
const qreal HEIGHT=sqrt(3.)*EDGE_LENGTH/2.;


class TextItem:public QGraphicsItem
{
public:
  enum Consts{penSize=5};

  TextItem(qreal x_, qreal y_, const QString &text_);
  QRectF boundingRect() const;
  void paint(QPainter *painter, const QStyleOptionGraphicsItem *option,
               QWidget *widget);
private:
  QString _text;
};

class DrawerEngine
{
public:
  DrawerEngine(QGraphicsScene* scene_ = 0);
  void setScene(QGraphicsScene* scene_);

  void drawSimulation(Simulation& simulation_, bool showPressure = false,
                      bool withField_ = true, bool withSensorRadius_ = false);
  void drawField(const Field& field_);
  void drawSensor(const Sensor& sensor_, bool showPressure = false,
                  const QVector<Arrow> &arrows_=QVector<Arrow>(), bool withSensorRadius_=false);

  Position getPosition(const QPointF& pt);

  QPointF calculatePosition(const Position& pos_);

private:
  void drawArrow(const Position& from, const Position& to);
  void drawArrow(const Arrow& arrow_, qreal x_, qreal y_);
  void drawTT(int layers_,qreal centerX_=0,qreal centerY_=0,
              bool sectors_=true, const QPen &pen_=QPen());

  void drawPoligon( const QVector<QPointF> & points_,
                    const QPen & pen_ = QPen(), const QBrush & brush_ = QBrush());

  QGraphicsScene* _scene;
};

#endif // DRAWERENGINE_H

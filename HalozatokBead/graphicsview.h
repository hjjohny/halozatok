#ifndef GRAPHICSVIEW_H
#define GRAPHICSVIEW_H

#include <QGraphicsView>
#include "position.h"

class GraphicsView : public QGraphicsView
{
    Q_OBJECT
public:
    explicit GraphicsView(QWidget *parent = 0);
protected:
    void mouseReleaseEvent (QMouseEvent *event);
signals:
    void clickAt(const QPointF& pos);
public slots:
    
};

#endif // GRAPHICSVIEW_H

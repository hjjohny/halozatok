#include "sensorgraphicsitem.h"
#include<QPainter>
#include <sstream>

SensorGraphicsItem::SensorGraphicsItem(qreal x_, qreal y_, int index_, int radius_, qreal penWidth_, QColor color)
    :_index(index_),_radius(radius_),_penWidth(penWidth_),_color(color)
{
  setPos(x_,y_);
}


QRectF SensorGraphicsItem::boundingRect() const
{
    return QRectF(x()-_radius,y()-_radius,
                 _radius*2,_radius*2);
}

void SensorGraphicsItem::paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget)
{
  QPen oldPen=painter->pen();
  QBrush oldBrush=painter->brush();
  painter->setBrush(QBrush(_color));

  painter->drawEllipse(boundingRect().x(),boundingRect().y(),_radius*2,_radius*2);
  painter->setPen(QPen(QBrush(Qt::black),_radius));
  std::stringstream ss;
  ss << _index;
  painter->drawText(boundingRect().x()+_radius-MAGIC_NUMBER,
                    boundingRect().y()+_radius+MAGIC_NUMBER,
                    QString(ss.str().c_str()));

  painter->setBrush(oldBrush);
  painter->setPen(oldPen);
}


